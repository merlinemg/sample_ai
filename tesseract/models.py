# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


# Create your models here.
class Tess(models.Model):
    tess_name = models.CharField(max_length=200)
    tess_file = models.FileField()

    def __str__(self):
        return self.tess_name
