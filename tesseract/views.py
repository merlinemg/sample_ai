from __future__ import unicode_literals
import csv
import json
import numpy
import numpy as np
import os
import PIL
import cv2
import pytesseract
import re
from PIL import Image, ImageDraw, ImageFont, ImageEnhance, ImageFilter
import PIL.ImageOps
from django.http import HttpResponse
from django.shortcuts import render
# Create your views here.
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from pyhon_tesseract.settings import BASE_DIR
from tesseract.forms import TessForm
from tesseract.models import Tess


class TessView(TemplateView):
    template_name = 'tess.html'

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(TessView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TessView, self).get_context_data(**kwargs)
        context['form'] = TessForm()
        return context

    def post(self, request):
        form = TessForm(request.POST, request.FILES)
        response_data = {}
        if request.is_ajax():
            data = request.POST.get('ajax_key')
            if data == 'upload_key':
                if form.is_valid():
                    try:
                        if Tess.objects.get(tess_name=request.POST.get('tess_name')):
                            response_data['status'] = False
                            response_data['status'] = form.errors
                    except:
                        form.save()
                        tess_instance = Tess.objects.get(tess_name=request.POST.get('tess_name'))
                        file_path = tess_instance.tess_file.url
                        # print os.path.join(BASE_DIR)
                        # print os.path.join(BASE_DIR) + file_path
                        # inverted_image = PIL.ImageOps.invert(Image.open(os.path.join(BASE_DIR) + file_path))
                        # print inverted_image, "///////////////////////////////////"
                        # # read_file = cv2.imread(os.path.join(BASE_DIR)+file_path, cv2.COLOR_BGR2GRAY)
                        # img = cv2.imread(os.path.join(BASE_DIR, file_path))
                        # print img, ".................."
                        # cv_file = cv2.UMat(img)
                        # print cv_file, "ppppppppppppp"

                        # load the example image and convert it to grayscale
                        # image = cv2.imread(os.path.join(BASE_DIR) + file_path)
                        # gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


                        # from PIL import Image
                        # image_file = Image.open("/home/user/project/pyhon_tesseract/crdr.bmp")  # open colour image
                        # image_file = image_file.convert('1')  # convert image to black and white
                        # image_file.save('result.png')

                        # img = cv2.imread("/home/user/project/pyhon_tesseract/crdr.bmp")
                        # # print pytesseract.image_to_string(Image.fromarray(img))
                        # img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                        # img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
                        #
                        # # Remove some noise
                        # kernel = np.ones((2, 1), np.uint8)
                        # img = cv2.dilate(img, kernel, iterations=1)
                        # img = cv2.erode(img, kernel, iterations=1)
                        # filename = "noise{}.jpg".format(os.getpid())
                        # cv2.imwrite(filename, img)
                        img = cv2.imread(os.path.join(BASE_DIR) + file_path)
                        # print pytesseract.image_to_string(Image.fromarray(img))
                        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                        img = cv2.threshold(img, 147, 255, cv2.THRESH_BINARY_INV | cv2.ADAPTIVE_THRESH_MEAN_C)[1]
                        # img = cv2.Sobel(img, cv2.CV_64F, 1, 0, ksize=5)
                        # img = cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize=5)


                        # make a check to see if median blurring should be done to remove
                        # noise
                        # elif args["preprocess"] == "blur":
                        # gray1 = cv2.medianBlur(img, 3)
                        # gray1 = cv2.normalize(img, 5)
                        # gray1 = cv2.pow(img, 1)
                        # gray1 = cv2.PSNR(img, img)
                        # gray1 = cv2.pyrDown(img)
                        # gray1 = cv2.pyrUp(img)  # good
                        # gray1 = cv2.reduce(img, 3, 3)
                        # gray1 = cv2.sort(img, 1)
                        # gray1 = cv2.blur(img, (3,2))
                        # gray1 = cv2.sortIdx(img, 1)
                        # gray1 = cv2.stylization(img)
                        # gray1 = cv2.sumElems(img)
                        # gray1 = cv2.SVDecomp(img)
                        # gray1 = cv2.transpose(img)

                        # write the grayscale image to disk as a temporary file so we can
                        # apply OCR to it
                        filename = "zoom{}.jpg".format(os.getpid())
                        print filename, ";';;;;;;;;;;"
                        # print filename
                        cv2.imwrite(filename, img)
                        # read_img = pytesseract.image_to_string(Image.open(os.path.join(BASE_DIR) + file_path),
                        #                                        config='-c preserve_interword_spaces=1 -psm3',
                        #                                        lang='eng')
                        # -c preserve_interword_spaces=1
                        # print read_img

                        # read_imgs = pytesseract.image_to_string(img,
                        #                                         config='-psm3 pdf',
                        #                                         lang='eng')
                        # print read_imgs

                        # from wand.image import Image as Img

                        # with Img(filename='/home/user/project/pyhon_tesseract/corporate-meals-packages.pdf') as img:
                        #     # img.compression_quality = 99
                        #     img.save(filename='image_name.jpg')
                        # print BASE_DIR + '/' + filename
                        img = cv2.imread(BASE_DIR + '/' + filename)
                        mser = cv2.MSER_create()
                        # x = []
                        # for row in range(img.shape[0]):
                        #     for col in range(img.shape[1]):
                        #         r, g, b = img[row, col]
                        #         print img[row, col]
                        # x.append(r)
                        # print x

                        # Resize the image so that MSER can work better
                        img = cv2.resize(img, (img.shape[1] * 2, img.shape[0] * 2))
                        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                        # gray = cv2.GaussianBlur(img, (5, 5), 0)
                        # gray = cv2.bilateralFilter(img,9,75,75)
                        vis = img.copy()
                        regions = mser.detectRegions(gray)
                        hulls = [cv2.convexHull(p.reshape(-1, 1, 2)) for p in regions[0]]
                        cv2.polylines(vis, hulls, 1, (0, 255, 0))
                        # cv2.namedWindow('img', 0)
                        # cv2.imshow('img', vis)
                        # while (cv2.waitKey() != ord('q')):
                        #     continue
                        # cv2.destroyAllWindows()
                        # print vis
                        # img = img[5:-5, 5:-5, :]
                        # px = img[100, 100]
                        # qx = img[255, 255]
                        # print px, "xxxxxxxxxxxx"
                        # print qx, "qqqqqqqqqqq"
                        if img[100, 100, 0] == 0:
                            print "11111111111111111111"
                            result = pytesseract.image_to_string(img, config='-psm 6')
                        else:
                            print "22222222222222222222"
                            result = pytesseract.image_to_string(img, config='-psm 4')
                        # print result, "........."
                        # img = cv2.imread('/home/user/project/pyhon_tesseract/crdr.bmp')
                        #
                        # kernel = np.ones((2, 1), np.uint8)
                        # # img = cv2.dilate(img, kernel, iterations=1)
                        # img = cv2.erode(img, kernel, iterations=1, )
                        # gray1 = cv2.medianBlur(img, 1)
                        # # gray1 = cv2.normalize(img, 5)
                        # # gray1 = cv2.pow(img, 1)
                        # # gray1 = cv2.PSNR(img, img)
                        # # gray1 = cv2.pyrDown(img)
                        # # gray1 = cv2.pyrUp(img)  # good
                        # # gray1 = cv2.reduce(img, 3, 3)
                        # # gray1 = cv2.sort(img, 1)
                        # # gray1 = cv2.blur(img, (3,2))
                        # # gray1 = cv2.sortIdx(img, 1)
                        # # gray1 = cv2.stylization(img)
                        # # gray1 = cv2.sumElems(img)
                        # # gray1 = cv2.SVDecomp(img)
                        # # gray1 = cv2.transpose(img)
                        # filename = "ppp{}.jpg".format(os.getpid())
                        # cv2.imwrite(filename, gray1)

                        # gray = cv2.imread('/home/user/project/pyhon_tesseract/crdr.bmp')
                        # edges = cv2.Canny(gray, 50, 150, apertureSize=3)
                        # cv2.imwrite('edges-50-150.jpg', edges)
                        # minLineLength = 100
                        # lines = cv2.HoughLinesP(image=edges, rho=1, theta=np.pi / 180, threshold=100,
                        #                         lines=np.array([]), minLineLength=minLineLength, maxLineGap=80)
                        #
                        # a, b, c = lines.shape
                        # for i in range(a):
                        #     cv2.line(gray, (lines[i][0][0], lines[i][0][1]), (lines[i][0][2], lines[i][0][3]),
                        #              (0, 0, 255), 3, cv2.LINE_AA)
                        #     cv2.imwrite('houghlines5.jpg', gray)


                        # img = cv2.imread('/home/user/project/pyhon_tesseract/crdr.bmp')
                        # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                        #
                        # corners = cv2.goodFeaturesToTrack(gray, 25, 0.01, 10)
                        # corners = np.int0(corners)
                        #
                        # # for i in corners:
                        # # x, y = i.ravel()
                        # cv2.clipLine(img, 300, 200)
                        # cv2.imwrite('vvvv1jr.jpg', img)


                        # im = cv2.imread('/home/user/project/pyhon_tesseract/crdr.bmp')
                        # rows, cols = im.shape[:2]
                        # imgray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
                        # ret, thresh = cv2.threshold(imgray, 125, 255, 0)
                        # thresh = (255 - thresh)
                        # thresh2 = thresh.copy()
                        # im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
                        #
                        # cv2.imshow('image1', im)
                        # cv2.imshow('image3', thresh2)
                        # # cv2.drawContours(im, contours, -1, (0,255,0), 3) #draw all contours
                        # contnumber = 4
                        # cv2.drawContours(im, contours, contnumber, (0, 255, 0), 3)  # draw only contour contnumber
                        # cv2.imshow('contours', im)
                        #
                        # [vx, vy, x, y] = cv2.fitLine(contours[contnumber], cv2.DIST_L2, 0, 0.01, 0.01)
                        # lefty = int((-x * vy / vx) + y)
                        # righty = int(((cols - x) * vy / vx) + y)
                        # cv2.line(im, (cols - 1, righty), (0, lefty), (0, 255, 255), 2)
                        #
                        # cv2.imshow('result', im)

                        # cv2.waitKey(0)
                        # cv2.destroyAllWindows()

                        # img = cv2.imread('/home/user/project/pyhon_tesseract/crdr.bmp')
                        # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                        # edges = cv2.Canny(gray, 50, 150, apertureSize=3)
                        #
                        # lines = cv2.HoughLines(edges, 1, np.pi / 180, 200)
                        # for rho, theta in lines[0]:
                        #     a = np.cos(theta)
                        #     b = np.sin(theta)
                        #     x0 = a * rho
                        #     y0 = b * rho
                        #     x1 = int(x0 + 1000 * (-b))
                        #     y1 = int(y0 + 1000 * (a))
                        #     x2 = int(x0 - 1000 * (-b))
                        #     y2 = int(y0 - 1000 * (a))
                        #
                        #     cv2.line(img, (x1, y1), (x2, y2), (0, 0, 255), 2)
                        #
                        # cv2.imwrite('houghlines3.jpg', img)




                        # img = cv2.imread('/home/user/project/pyhon_tesseract/Untitled Folder/12788.png')
                        # rows, cols, ch = img.shape
                        # pts1 = np.float32([[56, 65], [368, 52], [28, 387], [389, 390]])
                        # pts2 = np.float32([[0, 0], [300, 0], [0, 300], [300, 300]])
                        # M = cv2.getPerspectiveTransform(pts1, pts2)
                        # dst = cv2.warpPerspective(img, M, (300, 300))
                        # cv2.imwrite('nnnlh.jpg', dst)
                        # zzz = pytesseract.image_to_string(dst)
                        # print zzz..




                        # im = cv2.imread('/home/user/project/pyhon_tesseract/zoom24333.jpg')
                        # imgray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
                        #
                        # ret, thresh = cv2.threshold(imgray, 127, 255, 0)
                        # im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
                        # # cc = cv2.drawContours(im, contours, -1, (0, 255, 0), 3)
                        # # ccc = cv2.drawContours(im, contours, 3, (0, 255, 0), 3)
                        # cnt = contours[0]
                        # M = cv2.moments(cnt)
                        # print M, "mmmmmmmmmmmmmmm"
                        # area = cv2.contourArea(cnt)
                        # cv2.imwrite('area.png', area)
                        # epsilon = 0.1 * cv2.arcLength(cnt, True)
                        # approx = cv2.approxPolyDP(cnt, epsilon, True)
                        # print approx, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

                        # cccc = cv2.drawContours(im, [cnt], 0, (0, 255, 0), 3)
                        # cv2.imwrite('mmmmmmmmmmm.png', cc)
                        # cv2.imwrite('ccccc.png', ccc)
                        # area = cv2.contourArea(cnt)
                        # print im2, ".................."

                        # print data1, "11111111"
                        # print data2, "22222222"
                        #
                        # for i in data1:
                        #     print i, "000000000000000000"
                        array_data = []
                        newlines = result.split('\n')
                        for line in newlines:
                            line = line.split(' ')
                            array_data.append(line)

                        arrayy = []
                        result_array = result.split('\n')
                        for row in result_array:
                            # re.sub(' + ', 'x', row)
                            # row=' '.join(row.split())
                            # print row, "///////////"
                            # row_array = row.split(' ')
                            # print row_array, ".........."
                            # print row_array, "xcbvcvbvcbcbvcb"
                            # for word in row_array:
                            #     word
                            # print len(p)
                            # for x in range(0, len(p) - 1):
                            #     if p[x]:
                            #         if p[x + 1] == '' and p[x + 2]:
                            #             p[x] += p[x + 2]
                            #             # print p[x]
                            #             # x = x + 2
                            #     # arrayy.append(p[x])
                            #     # y.append(p[x])
                            #
                            #     # elif p[x + 1] == '' and p[x + 2] == '':
                            #     #     # print p[x]
                            #     #     y.append(p[x])

                            # print y
                            # for j in range(0, len(i.split(' '))):
                            # print i[j], "OOOOOOOOOOOOOO"
                            space = re.findall('\s+', row)
                            for range_len in range(0, len(space)):
                                space_len = len(space[range_len])
                                if space_len >= 10:
                                    val = space_len / 8
                                    for n in range(0, val):
                                        print space[range_len], "................."
                                        row = row.replace(space[range_len], " ** ")
                                        print row, "oooooooooo"
                                        # if m<10 and m>1:
                                        #     row = row.replace(space[k], " ##")
                            # m = len(space[k])
                            # if m >= 18:
                            #     # print m
                            #     # print m
                            #     # print k,"kkkkkkkkkkkkkkkkkkkkkkkkkkk"
                            #
                            #     # row = re.sub('  +', '  ', row)
                            #
                            #     # print m
                            #     v = m / 18
                            #     for p in range(0, v - 1):
                            #         # row = row.split()[0]
                            #         print ",,,,,"
                            # print row, "////////////"
                            # row_array = row.split(' ')
                            #     row = row.replace(space[k], " ")
                            row_array = row.split(' ')
                            arrayy.append(row_array)
                            # arrayy.append(row)
                        # print arrayy, "788888888888888887"
                        # print arrayy, ",,,,,,,,,,,,,,,,"
                        # for i in arrayy:


                        # i = i.split(' ')
                        # print i[0]
                        # y.append(i)

                        # -c preserve_interword_spaces = 1

                        # read_img_1 = pytesseract.image_to_string(Image.open(os.path.join(BASE_DIR) + file_path),
                        #                                          config='hocr', lang='eng', boxes=False)
                        # print read_img_1, "111111111"
                        #
                        # read_img_2 = pytesseract.image_to_string(Image.open(os.path.join(BASE_DIR) + file_path),
                        #                                          config='-psm 6', lang='eng', boxes=False)
                        # print read_img_2, "22222222222222"
                        #
                        # read_img_3 = pytesseract.image_to_string(Image.open(os.path.join(BASE_DIR) + file_path),
                        #                                          config='-psm 3', lang='eng', boxes=False)
                        # print read_img_3, "33333333333333333333"

                        # read_img_4 = pytesseract.pytesseract.image_to_string(
                        #     Image.open(os.path.join(BASE_DIR) + file_path),
                        #     config='-psm 9', lang='eng', boxes=False)
                        # print read_img_4, "4444444444444444444"
                        #
                        # read_img_5 = pytesseract.pytesseract.image_to_string(
                        #     Image.open(os.path.join(BASE_DIR) + file_path),
                        #     config='-c tessedit_char_whitelist=abcdefghijklmnopqrstuvwxyz', lang='eng', boxes=False)
                        # print read_img_5, "5555555555555555555555555"
                        #
                        # read_img_6 = pytesseract.pytesseract.image_to_string(
                        #     Image.open(os.path.join(BASE_DIR) + file_path),
                        #     config='outputbase digits', lang='eng',
                        #     boxes=False)
                        # print read_img_6, "6666666666666666666"

                        # with open('files.csv', 'wb') as csv_file:
                        #     writer = csv.writer(csv_file)
                        #     writer.writerow(['Product Name', 'Product Price'])
                        #     x = []
                        #     for item in range(0, len(phone_name)):
                        #         x.append(phone_name[item] + phone_price[item])
                        #         writer.writerow([phone_name[item], phone_price[item]])


                        # check to see if we should apply thresholding to preprocess the
                        # image
                        # if args["preprocess"] == "thresh":


                        # im = im.resize(size_tuple)

                        # import tesserocr
                        # # from PIL import Image
                        #
                        # print tesserocr.tesseract_version()  # print tesseract-ocr version
                        # print tesserocr.get_languages()  # prints tessdata path and list of available languages

                        # image = Image.open('/home/user/project/pyhon_tesseract/ccccccccccccccccccccccc.png'),"------------"
                        # print tesserocr.image_to_text(image)  # print ocr text from image
                        # # or
                        # print tesserocr.file_to_text('/home/user/project/pyhon_tesseract/ccccccccccccccccccccccc.png'), "8888888888888888888"

                        # x = []
                        # newline = read_img.split('\n')
                        # # length = len(newline[0])
                        # # print length, "..........."
                        # for i in newline:
                        #     i = i.split(' ')
                        #     x.append(i)
                        response_data['html_content'] = render_to_string('ajax/tess_data.html',
                                                                         {'read_img': array_data, 'row_array': arrayy})
                        response_data['status'] = True
        return HttpResponse(json.dumps(response_data), content_type="application/json")

