from .import views
from django.conf.urls import url

urlpatterns = [
    url(r'^', views.TessView.as_view(), name='tess'),
]