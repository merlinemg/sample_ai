from django import forms

from tesseract.models import Tess


class TessForm(forms.ModelForm):
    class Meta:
        model = Tess
        fields = '__all__'

    def clean(self):
        tess_name = self.cleaned_data.get("tess_name")
        # print tess_name, "hhhhhhhhhhhhhhhhhhhhhhhhh"
        try:
            if Tess.objects.get(tess_name=tess_name).exists():
                # print "qqqqqqqqqqqqqqqqqqqqqqqqqqqq"
                raise forms.ValidationError("name alredy exist...")
        except:
            pass
        return self.cleaned_data
